<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = task::all();
        return response()->json($tasks);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'date' => 'required',
            'status' => 'required'
        ]);

        if($validated->fails()) {
            return response()->json([
                'message' => 'Sua request está faltando dados'
            ], 400);
        }

        $task = task::create($request->all());

        return response()->json([
            'message' => 'task criada!',
            'task' => $task,
        ], 201);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {

        return response()->json([
            'message' => 'Feito (;',
            'task' => $task
        ], 201);
        
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {

        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'date' => 'required',
            'status' => 'required'
        ]);

        if($validated->fails()) {
            return response()->json([
                'message' => 'Sua request está faltando dados'
            ], 400);
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {

        $task->delete();

        return response()->json([
            'message' => "Task deletada!",
        ], 200);
        //
    }
}
